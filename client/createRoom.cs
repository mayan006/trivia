﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class createRoom : Form
    {
        public createRoom()
        {
            InitializeComponent();
        }

        private void btnSignIn_Click(object sender, EventArgs e)
        {

        }

        private void logoPbx_Click(object sender, EventArgs e)
        {

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> massage = new Dictionary<string, string>();
            massage["roomName"] = roomName.Text;
            massage["questionCount"] = numOfQuestions.Text;
            massage["maxUsers"] = numOfPlayers.Text; 
            massage["answerTimeout"] = mailTxb.Text;
            string strMassage = clientCom.DictToStr(massage);
            string answer = clientCom.communicator.sendAndGet(clientCom.Stutes.Creat, strMassage);
            if (answer != "")
            {
                this.Close();
                waitingRoomAdmin roomAdmin = new waitingRoomAdmin();
                roomAdmin.ShowDialog();
                
            }
        }

    }
}
