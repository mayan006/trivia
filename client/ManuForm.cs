﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class ManuForm : Form
    {
        static public string userName = "";
        Button[] userBtn = new Button[4];
        Color orange = Color.FromArgb(192, 64, 0);
        List<Control> signControl = new List<Control>();
        public ManuForm()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            logoPbx.Visible = true;
            FormClosed += new FormClosedEventHandler(closeSocket);
            userBtn[0] = btnJoin;
            userBtn[1] = btnCreate;
            userBtn[2] = btnStatus;
            userBtn[3] = btnBest;
            signControl.Add(username);
            signControl.Add(password);
            signControl.Add(labelUsername);
            signControl.Add(labelPasswords);
            signControl.Add(btnLogin);
            signControl.Add(btnSignUp);
            signControl.Add(btnSignOut);
            signControl.Add(lblHello);
            signControl.Add(lblUser);
            foreach(Button button in userBtn) button.BackColor = Color.Gray;
        }
        private void closeSocket(object sender, EventArgs e)
        {
            clientCom.communicator.closeSocket();
        }

        private void username_TextChanged(object sender, EventArgs e)
        {

        }

        private void password_TextChanged(object sender, EventArgs e)
        {

        }

        private void logoPbx_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> massage = new Dictionary<string, string>();
            massage["username"] = username.Text;
            massage["password"] = password.Text;
            string strMassage = clientCom.DictToStr(massage);
            string answer = clientCom.communicator.sendAndGet(0, strMassage);
            if (answer != "")
            {
                lblUser.Text = username.Text;
                lblHello.Text = "Hello " + username.Text;
                foreach (var button in userBtn) button.BackColor = orange;
                foreach (var control in signControl) control.Visible = !control.Visible;
                userName = username.Text;
            }
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBest_Click(object sender, EventArgs e)
        {
            if (btnBest.BackColor == orange)
            {

            }
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            SignUpForm signup = new SignUpForm();
            signup.ShowDialog();


        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnJoin_Click(object sender, EventArgs e)
        {
            if(btnJoin.BackColor == orange)
            {
                joinRoom join = new joinRoom();
                join.ShowDialog();
            }
        }

        private void btnStatus_Click(object sender, EventArgs e)
        {
            if (btnStatus.BackColor == orange)
            {

            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (btnCreate.BackColor == orange)
            {
                createRoom create = new createRoom();
                create.ShowDialog();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> massage = new Dictionary<string, string>();
            massage["stustes"] = "1";
            string strMassage = clientCom.DictToStr(massage);
            string answer = clientCom.communicator.sendAndGet(clientCom.Stutes.logout, strMassage);
            foreach (var button in userBtn) button.BackColor = Color.Gray;
            foreach (var control in signControl) control.Visible = !control.Visible;
        }
    }
}
