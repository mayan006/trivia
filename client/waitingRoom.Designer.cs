﻿namespace client
{
    partial class waitingRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLeave = new System.Windows.Forms.Button();
            this.playersList = new System.Windows.Forms.ListBox();
            this.logoPbx = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.logoPbx)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLeave
            // 
            this.btnLeave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnLeave.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLeave.ForeColor = System.Drawing.Color.Black;
            this.btnLeave.Location = new System.Drawing.Point(367, 199);
            this.btnLeave.Name = "btnLeave";
            this.btnLeave.Size = new System.Drawing.Size(102, 39);
            this.btnLeave.TabIndex = 15;
            this.btnLeave.Text = "Leave";
            this.btnLeave.UseVisualStyleBackColor = false;
            this.btnLeave.Click += new System.EventHandler(this.btnLeave_Click);
            // 
            // playersList
            // 
            this.playersList.Location = new System.Drawing.Point(100, 180);
            this.playersList.Name = "playersList";
            this.playersList.Size = new System.Drawing.Size(250, 100);
            this.playersList.TabIndex = 19;
            // 
            // logoPbx
            // 
            this.logoPbx.ErrorImage = null;
            this.logoPbx.ImageLocation = "logo";
            this.logoPbx.InitialImage = global::client.Properties.Resources.logo;
            this.logoPbx.Location = new System.Drawing.Point(107, 12);
            this.logoPbx.Name = "logoPbx";
            this.logoPbx.Size = new System.Drawing.Size(242, 130);
            this.logoPbx.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logoPbx.TabIndex = 12;
            this.logoPbx.TabStop = false;
            // 
            // waitingRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(481, 307);
            this.Controls.Add(this.btnLeave);
            this.Controls.Add(this.logoPbx);
            this.Controls.Add(this.playersList);
            this.Name = "waitingRoom";
            this.Text = "waitingRoom";
            ((System.ComponentModel.ISupportInitialize)(this.logoPbx)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLeave;
        private System.Windows.Forms.PictureBox logoPbx;
        private System.Windows.Forms.ListBox playersList;
    }
}