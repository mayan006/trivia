﻿namespace client
{
    partial class createRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSend = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numOfQuestions = new System.Windows.Forms.TextBox();
            this.numOfPlayers = new System.Windows.Forms.TextBox();
            this.timeForQuestion = new System.Windows.Forms.Label();
            this.mailTxb = new System.Windows.Forms.TextBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.roomName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.logoPbx = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.logoPbx)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSend
            // 
            this.btnSend.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnSend.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSend.ForeColor = System.Drawing.Color.Black;
            this.btnSend.Location = new System.Drawing.Point(367, 199);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(102, 39);
            this.btnSend.TabIndex = 15;
            this.btnSend.Text = "SEND";
            this.btnSend.UseVisualStyleBackColor = false;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.Location = new System.Drawing.Point(27, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 20);
            this.label3.TabIndex = 14;
            this.label3.Text = "num of players:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.Location = new System.Drawing.Point(27, 216);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "num of questions:";
            // 
            // numOfQuestions
            // 
            this.numOfQuestions.Location = new System.Drawing.Point(159, 216);
            this.numOfQuestions.Name = "numOfQuestions";
            this.numOfQuestions.Size = new System.Drawing.Size(190, 20);
            this.numOfQuestions.TabIndex = 11;
            // 
            // numOfPlayers
            // 
            this.numOfPlayers.Location = new System.Drawing.Point(143, 185);
            this.numOfPlayers.Name = "numOfPlayers";
            this.numOfPlayers.Size = new System.Drawing.Size(206, 20);
            this.numOfPlayers.TabIndex = 10;
            // 
            // timeForQuestion
            // 
            this.timeForQuestion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.timeForQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.timeForQuestion.Location = new System.Drawing.Point(27, 243);
            this.timeForQuestion.Name = "timeForQuestion";
            this.timeForQuestion.Size = new System.Drawing.Size(135, 26);
            this.timeForQuestion.TabIndex = 17;
            this.timeForQuestion.Text = "timeForQuestion:";
            // 
            // mailTxb
            // 
            this.mailTxb.Location = new System.Drawing.Point(159, 249);
            this.mailTxb.Name = "mailTxb";
            this.mailTxb.Size = new System.Drawing.Size(190, 20);
            this.mailTxb.TabIndex = 16;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.Sienna;
            this.btnBack.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnBack.Location = new System.Drawing.Point(394, 12);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 31);
            this.btnBack.TabIndex = 18;
            this.btnBack.Text = "back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // roomName
            // 
            this.roomName.Location = new System.Drawing.Point(116, 152);
            this.roomName.Name = "roomName";
            this.roomName.Size = new System.Drawing.Size(233, 20);
            this.roomName.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.Location = new System.Drawing.Point(27, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 20);
            this.label4.TabIndex = 17;
            this.label4.Text = "roomName:";
            // 
            // logoPbx
            // 
            this.logoPbx.ErrorImage = null;
            this.logoPbx.ImageLocation = "logo";
            this.logoPbx.InitialImage = global::client.Properties.Resources.logo;
            this.logoPbx.Location = new System.Drawing.Point(107, 12);
            this.logoPbx.Name = "logoPbx";
            this.logoPbx.Size = new System.Drawing.Size(242, 130);
            this.logoPbx.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logoPbx.TabIndex = 12;
            this.logoPbx.TabStop = false;
            this.logoPbx.Click += new System.EventHandler(this.logoPbx_Click);
            // 
            // createRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(481, 307);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.timeForQuestion);
            this.Controls.Add(this.mailTxb);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.logoPbx);
            this.Controls.Add(this.numOfQuestions);
            this.Controls.Add(this.numOfPlayers);
            this.Controls.Add(this.roomName);
            this.Controls.Add(this.label4);
            this.Name = "createRoom";
            this.Text = "createRoom";
            ((System.ComponentModel.ISupportInitialize)(this.logoPbx)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox logoPbx;
        private System.Windows.Forms.TextBox numOfPlayers;
        private System.Windows.Forms.TextBox numOfQuestions;
        private System.Windows.Forms.Label timeForQuestion;
        private System.Windows.Forms.TextBox mailTxb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox roomName;
        private System.Windows.Forms.Button btnBack;
    }
}
