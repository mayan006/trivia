﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{

    class clientCom
    {
        public enum Stutes { login, signup, Error, logout, join, Creat, GetRoom, Getplayers, High, close, start, Status, leave, Ok };
        bool open = false;
        NetworkStream stream;
        TcpClient client;
        static public clientCom communicator = new clientCom();

        private clientCom()
        {
            openC();
        }
        public void openC(){
            try
            {
                client = new TcpClient("127.0.0.1", 1200);
                stream = client.GetStream();
                open = true;
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public string sendAndGet(Stutes code, string message)
        {
            if (!open) openC();
            if (open)
            {
                message = (int)code + To4Bites(message.Length) + message;
                byte[] data = Encoding.ASCII.GetBytes(message);
                stream.Write(data, 0, data.Length);
                data = new byte[1024];
                string responseData;
                int bytes;
                try
                {
                    bytes = stream.Read(data, 0, data.Length);
                }
                catch (Exception a)
                {
                    open = false;
                    MessageBox.Show(a.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return "";
                }
                int codeRes = Convert.ToInt32(data[0]) - 48;
                responseData = Encoding.ASCII.GetString(data, 3, bytes - 4);
                if (codeRes == (int)Stutes.Error) { MessageBox.Show(responseData, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return "";
                }
                return responseData;
            }
            else return "";
          
        }

        public void closeSocket()
        {
            if (open)
            {
                try
                {
                    //stream.Close();
                    //client.Close();
                }
                catch (Exception a)
                {
                    MessageBox.Show(a.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                open = false;
            }
        }

        public static string To4Bites(int len)
        {
            string lenStr = len.ToString();
            while(lenStr.Length < 4) lenStr = "0" + lenStr;
            
            return lenStr;
        }

        public static string DictToStr(Dictionary<string, string> dictionary)
        {
            return "{" + string.Join(",", dictionary.Select(kv => (kv.Key + ":" + kv.Value)).ToArray()) + "}";
        }
    }
}
