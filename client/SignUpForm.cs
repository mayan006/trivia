﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class SignUpForm : Form
    {
        public SignUpForm()
        {
            InitializeComponent();
        }

        private void signup_Load(object sender, EventArgs e)
        {

        }

        private void username_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void mailTxb_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> massage = new Dictionary<string, string>();
            massage["username"] = username.Text;
            massage["password"] = password.Text;
            massage["mail"] = mailTxb.Text;
            string strMassage = clientCom.DictToStr(massage);
            string answer = clientCom.communicator.sendAndGet(clientCom.Stutes.signup, strMassage);
            if (answer != "") Close();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void logoPbx_Click(object sender, EventArgs e)
        {

        }
    }
}
