﻿namespace client
{
    partial class waitingRoomAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCloseRoom = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.playersList = new System.Windows.Forms.ListBox();
            this.logoPbx = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.logoPbx)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCloseRoom
            // 
            this.btnCloseRoom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnCloseRoom.Font = new System.Drawing.Font("Arial Narrow", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCloseRoom.ForeColor = System.Drawing.Color.Black;
            this.btnCloseRoom.Location = new System.Drawing.Point(372, 160);
            this.btnCloseRoom.Name = "btnCloseRoom";
            this.btnCloseRoom.Size = new System.Drawing.Size(90, 30);
            this.btnCloseRoom.TabIndex = 26;
            this.btnCloseRoom.Text = "close room";
            this.btnCloseRoom.UseVisualStyleBackColor = false;
            this.btnCloseRoom.Click += new System.EventHandler(this.btnCloseRoom_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnStart.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.ForeColor = System.Drawing.Color.Black;
            this.btnStart.Location = new System.Drawing.Point(367, 199);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(102, 39);
            this.btnStart.TabIndex = 15;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // roomList
            // 
            this.playersList.Location = new System.Drawing.Point(100, 180);
            this.playersList.Name = "playersList";
            this.playersList.Size = new System.Drawing.Size(250, 100);
            this.playersList.TabIndex = 19;
            // 
            // logoPbx
            // 
            this.logoPbx.ErrorImage = null;
            this.logoPbx.ImageLocation = "logo";
            this.logoPbx.InitialImage = global::client.Properties.Resources.logo;
            this.logoPbx.Location = new System.Drawing.Point(107, 12);
            this.logoPbx.Name = "logoPbx";
            this.logoPbx.Size = new System.Drawing.Size(242, 130);
            this.logoPbx.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logoPbx.TabIndex = 12;
            this.logoPbx.TabStop = false;
            // 
            // waitingRoomAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(481, 307);
            this.Controls.Add(this.btnCloseRoom);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.logoPbx);
            this.Controls.Add(this.playersList);
            this.Name = "waitingRoomAdmin";
            this.Text = "waitingRoomAdmin";
            ((System.ComponentModel.ISupportInitialize)(this.logoPbx)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCloseRoom;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.PictureBox logoPbx;
        private System.Windows.Forms.ListBox playersList;
    }
}