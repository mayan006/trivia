﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace client
{
    public partial class joinRoom : Form
    {
        bool stop = true;
        public joinRoom()
        {
            InitializeComponent();
            Shown += joinRoom_Shown;
        }

        private void joinRoom_Shown(object sender, EventArgs e)
        {
            Task.Run(() =>
            {
                while (true)
                {
                    if(this.stop == false)
                    {
                        break;
                    }
                    btnRefresh.PerformClick();
                    Task.Delay(3000).Wait();
                }
            }
            );
          
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnJoin_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> massage = new Dictionary<string, string>();
            string text = roomList.GetItemText(roomList.SelectedItem);
            massage["roomId"] = text.Split('-')[1];
            string strMassage = clientCom.DictToStr(massage);
            string answer = clientCom.communicator.sendAndGet(clientCom.Stutes.join, strMassage);
            if (answer != "")
            {
                this.stop = false;
                waitingRoom room = new waitingRoom();
                room.ShowDialog();
                this.Close();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            roomList.Items.Clear();
            string answer = clientCom.communicator.sendAndGet(clientCom.Stutes.GetRoom, "");
            string[] _rooms = answer.Split(',');
            
            foreach (string i in _rooms)
            {
                roomList.Items.Add(i);
            }
        }
    }
}
