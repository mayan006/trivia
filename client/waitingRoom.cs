﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class waitingRoom : Form
    {
        bool stop = true;
        public waitingRoom()
        {
            InitializeComponent();
            Shown += waitingRoom_Shown;
        }

        private void waitingRoom_Shown(object sender, EventArgs e)
        {
            {
                Task.Run(() =>
                {
                    while (true)
                    {
                        if (this.stop == false)
                        {
                            break;
                        }
                        playersList.Items.Clear();
                        string answer = clientCom.communicator.sendAndGet(clientCom.Stutes.Status, "");
                        answer = answer.Split('[')[1];
                        string[] _players = answer.Split(',');

                        foreach (string i in _players)
                        {
                            playersList.Items.Add(i);
                        }
                        Task.Delay(3000).Wait();
                    }
                }
                );
            }
        }
        private void btnLeave_Click(object sender, EventArgs e)
        {
            
            string answer = clientCom.communicator.sendAndGet(clientCom.Stutes.leave, "");
            if (answer != "")
            {
                this.stop = false;
                this.Close();
            }
        }
    }
}
