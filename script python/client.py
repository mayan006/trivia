import socket


def send_reqest(cilent_soket, msg = None):
    """
    function show the menu and get ask from the user and send to the server, and get the answer
    :param IP:
    :param port:
    :return: null
    """
    try:
        print(msg)
        cilent_soket.sendall(msg)
    except ConnectionResetError or socket.timeout as e:
        print("the server is close :(")
        return 0

def get_reqest(cilent_soket):
    """
    function show the menu and get ask from the user and send to the server, and get the answer
    :param IP:
    :param port:
    :return: null
    """
    try:
        server_msg = cilent_soket.recv(1024)
    except socket.timeout as e:
        print("the server is close :(")
        return None
    server_msg = server_msg.decode()
    return server_msg


def login(clinet_socket, signup):
    username = input("input your username: ")
    password = input("input your password: ")
    if signup:
        email = input("input your email: ")
        massage_dict = {"username":username, "password": password, "email":email}
        code = 1
    else:
        massage_dict = {"username":username, "password":password}
        code = 0
    byte_massage = bytearray(code)
    byte_massage.append(len(str(massage_dict)))
    for byte in str(massage_dict).encode():
        byte_massage.append(byte)
    send_reqest(clinet_socket, byte_massage)



def main():
    listen_port = input("enter port: ")
    while not listen_port.isdigit() or 1024 > int(listen_port) or 65535 < int(listen_port):
        listen_port = input("wrong value you need to int input between 1024 to 65535, entet port: ")
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:
        server_address = ("127.0.0.1", int(listen_port))
        client_socket.settimeout(5)
        try:
            client_socket.connect(server_address)
        except socket.timeout and ConnectionRefusedError as e:
            print("the server is close :(")
            return 0
        toSignup = input("input 1 to signup\nelse to login: ") == "1"
        login(client_socket, toSignup)
        msg = get_reqest(client_socket)
        print(msg)

if __name__ == '__main__':
    main()
