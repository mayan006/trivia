#include "LoginRequestHandler.h"
#include "RequestHandlerFactory.h"

LoginRequestHandler::LoginRequestHandler(IRequestHandler* handler, ReqestHandleFactory* factory) :
IRequestHandler(handler, factory)
{
}

bool LoginRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
    return requestInfo.id == Login || requestInfo.id == Signup;
}

RequestResualt LoginRequestHandler::handleRequest(RequestInfo requestInfo)
{
    RequestResualt req;
    if (requestInfo.id == Login) {
        req = login(requestInfo);
    }
    if (requestInfo.id == Signup) {
        req = signin(requestInfo);
    }
    return req;
}

RequestResualt LoginRequestHandler::login(RequestInfo info)
{
    RequestResualt res;
    LoginRequest log = deserializer::deserializeLoginRequest(info.buffer);
    factory->getLoginManger()->login(log.username, log.password);
    res.response = serializer::serializeResponse(LoginResponse());
    if (handler) { 
        delete handler; 
        handler = nullptr;
    }
    res.newHandler = factory->createMenuRequestHandler(LoggedUser(log.username), this);
    return res;
}

RequestResualt LoginRequestHandler::signin(RequestInfo info)
{
    RequestResualt res;
    SignupRequest log = deserializer::deserializeSignupRequest(info.buffer);
    factory->getLoginManger()->signup(log.username, log.password, log.email);
    res.response = serializer::serializeResponse(SignupResponse());
    res.newHandler = this;
    return res;
}
