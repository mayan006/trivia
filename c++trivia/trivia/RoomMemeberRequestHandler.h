#pragma once
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "Room.h"

class RoomMemberRequestHandler : public IRequestHandler {
public:
	RoomMemberRequestHandler(LoggedUser user, Room room, IRequestHandler* handler, ReqestHandleFactory* factory);
	virtual bool isRequestRelevant(RequestInfo requestInfo);
	virtual RequestResualt handleRequest(RequestInfo requestInfo);
protected:
	RequestResualt LeaveRoom(RequestInfo reqestInfo);
	RequestResualt getState(RequestInfo reqestInfo);

	LoggedUser user;
	Room room;
};
