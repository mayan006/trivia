#pragma once
#include "IDataBase.h"
#include "LoggedUser.h"
class LoginManger {
public:
	LoginManger(IDataBase* dataDase);
	void signup(string username, string password, string mail);
	void login(string username, string password);
	void logout(string username);
	~LoginManger();
private:
	IDataBase* m_data_base;
	vector<LoggedUser> m_loggedUser;
};
