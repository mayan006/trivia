#include "JsonResponsePacketSerializer.h"
#include "Room.h"

vector<char> JsonRequestPacketSerializer::serializeResponse(LoginResponse log)
{
	return dataToBuffer(Login);
}

vector<char> JsonRequestPacketSerializer::serializeResponse(SignupResponse sig)
{
	return dataToBuffer(Signup);
}
vector<char> JsonRequestPacketSerializer::serializeResponse(ErrorResponse err)
{
	std::string data = "{message: " + err.message + "}";
	return dataToBuffer(Error, data);
}

vector<char> JsonRequestPacketSerializer::serializeResponse(LogoutResponse res)
{
	return dataToBuffer(Logout);
}


vector<char> JsonRequestPacketSerializer::serializeResponse(JoinRoom res)
{
	return dataToBuffer(Join);
}

vector<char> JsonRequestPacketSerializer::serializeResponse(CreateRoom res)
{
	return dataToBuffer(Create);
}

vector<char> JsonRequestPacketSerializer::serializeResponse(Rooms room)
{
	std::string data = "{rooms:";

	for (int i = 0;i < room.rooms.size();i++)
		data += room.rooms[i].name + "-" + to_string(room.rooms[i].id) + ",";
	data.pop_back();
	data  += '}';
	return dataToBuffer(GetRoom, data);
}

vector<char> JsonRequestPacketSerializer::serializeResponse(PlayersInRoom pir)
{
	std::vector<std::string> high = pir.players;
	std::string data = "{players:";
	std::string data2;
	for (std::vector<std::string>::const_iterator i = high.begin(); i != high.end(); ++i)
		data2 += *i + ",";
	data = data + data2 + '}';
	return dataToBuffer(GetPlayers, data);
}

vector<char> JsonRequestPacketSerializer::serializeResponse(UserStatistics ustat)
{
	std::vector<std::string> high = ustat.highscore;
	std::string data = "{HighScores:";
	std::string data2;
	for (std::vector<std::string>::const_iterator i = high.begin(); i != high.end(); ++i)
		data2 += *i;
	data = data + data2 + '}';
	return dataToBuffer(HighScore, data);
}

vector<char> JsonRequestPacketSerializer::serializeResponse(CloseRoomResponse close)
{
	return dataToBuffer(Close);
}

vector<char> JsonRequestPacketSerializer::serializeResponse(StartGameResponse start)
{
	return dataToBuffer(Start);
}

vector<char> JsonRequestPacketSerializer::serializeResponse(GetRoomStateResponse state)
{
	string data = "{";
	data += "\"status\":" + state.status;
	data += ", \"hasGameBegun\":" + state.hasGameBegun;
	data += ", \"players\": [";
	for (auto player : state.players) {
		data += "\"" + player + "\",";
	}
	data.pop_back();
	data += "], \"answerTimeOut\":" + state.answerTimeOut;
	return dataToBuffer(State, data);
}

vector<char> JsonRequestPacketSerializer::serializeResponse(LeaveRoomResponse leave)
{
	return dataToBuffer(Leave);
}

vector<char> JsonRequestPacketSerializer::dataToBuffer(int code, string data)
{
	std::vector<char> buffer;
	char len = data.length();
	for (char digit : to_string(code)) buffer.push_back(digit);
	buffer.push_back(len);
	buffer.insert(buffer.end(), data.begin(), data.end());
	return buffer;
}
