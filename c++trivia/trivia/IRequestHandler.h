#pragma once
#include"structs.h"

struct RequestResualt;
class ReqestHandleFactory;
class IRequestHandler {
public:
	IRequestHandler(IRequestHandler* handler, ReqestHandleFactory* factory);
	virtual bool isRequestRelevant(RequestInfo requestInfo) = 0;
	virtual RequestResualt handleRequest(RequestInfo requestInfo) = 0;
	virtual ~IRequestHandler();
protected:
	IRequestHandler* handler;
	ReqestHandleFactory* factory;
};
struct RequestResualt {
		IRequestHandler* newHandler;
		std::vector<char> response;
} typedef RequestResualt;