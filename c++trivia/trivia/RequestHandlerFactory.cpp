#include "RequestHandlerFactory.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"

ReqestHandleFactory::ReqestHandleFactory(LoginManger* loginManger, RoomManger* roomManger, 
	StatisticsManger* statstic, IDataBase* iDataBase) :
	loginManger(loginManger), roomManger(roomManger), statstic(statstic), iDataBase(iDataBase)
{
}

LoginRequestHandler* ReqestHandleFactory::createLoginRequestHandler(IRequestHandler* handler)
{
	return new LoginRequestHandler(handler, this);
}

MenuRequestHandler* ReqestHandleFactory::createMenuRequestHandler(LoggedUser user, IRequestHandler* handler)
{
	return new MenuRequestHandler(user, handler, this);
}

RoomAdminRequestHandler* ReqestHandleFactory::createAdminRequestHandler(LoggedUser user, Room room, IRequestHandler* handler)
{
	return new RoomAdminRequestHandler(user, room, handler, this);
}

RoomMemberRequestHandler* ReqestHandleFactory::createMemberRequestHandler(LoggedUser user, Room room, IRequestHandler* handler)
{
	return new RoomMemberRequestHandler(user, room, handler, this);
}

LoginManger* ReqestHandleFactory::getLoginManger()
{
	return loginManger;
}

RoomManger* ReqestHandleFactory::getRoomManger()
{
	return roomManger;
}

StatisticsManger* ReqestHandleFactory::getStatstic()
{
	return statstic;
}

IDataBase* ReqestHandleFactory::getIDataBase()
{
	return iDataBase;
}
