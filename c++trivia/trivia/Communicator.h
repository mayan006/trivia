#pragma once
#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <map>
#include "LoginRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "StatisticsManger.h"
#include <deque>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <WinSock2.h>
#include <exception>
#include <string>
#include <numeric>
#include <iostream>
#include <bitset>

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "1200"

class Communicator {
private:
	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;

	struct addrinfo* result = NULL;
	struct addrinfo hints;
	SOCKET m_serverSocket;
	std::map<SOCKET, LoginRequestHandler*> m_clients;
	ReqestHandleFactory* factory;
	bool isAaccept;
	//ReqestHandleFactory& m_handlerFactory;


	void bindAndListen();
	void handleNewClient();
public:
	Communicator();
	~Communicator();
	void startHandleRequests();
	RequestInfo getMassege(SOCKET client);
	LoginManger* loginManger;
	RoomManger* roomManger;
	StatisticsManger* statstic;
	IDataBase* dataBase;
};
