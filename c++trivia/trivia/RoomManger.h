#pragma once
#include "Room.h"
class RoomManger {
public:
	void createRoom(LoggedUser user, Room room);
	void deleteRoom(int roomID);
	Room* getRoom(int roomID);
	vector<RoomData> getRooms();
	Room joinRoom(LoggedUser user, int roomID);
	void LeaveRoom(LoggedUser user, int roomID);
	void StartGame(int roomID);
	int getId();
private:
	int roomId = 1;
	map<int, Room> rooms;
};