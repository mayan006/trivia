#pragma once
#include "structs.h"
static class JsonRequestPacketDeserializer {
public:
	static LoginRequest deserializeLoginRequest(vector<char> buffer);
	static SignupRequest deserializeSignupRequest(vector<char> buffer);


	static GetPlayersInRoomRequest deserializeGetPlayersInRoomRequest(vector<char> buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(vector<char> buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(vector<char> buffer);
	static map<string, string> splitJson(string json);
}; typedef JsonRequestPacketDeserializer deserializer;
