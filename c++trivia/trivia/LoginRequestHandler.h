#pragma once

#include "IRequestHandler.h"
#include "StatisticsManger.h"
#include "SqliteDataBase.h"
#include "RoomManger.h"
#include "IRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "LoginManger.h"
class LoginRequestHandler : public IRequestHandler {
public:
	LoginRequestHandler(IRequestHandler* handler, ReqestHandleFactory* factory);
	bool isRequestRelevant(RequestInfo RequestInfo);
	RequestResualt handleRequest(RequestInfo RequestInfo);
	~LoginRequestHandler() = default;
private:
	RequestResualt login(RequestInfo info);
	RequestResualt signin(RequestInfo info);
};
