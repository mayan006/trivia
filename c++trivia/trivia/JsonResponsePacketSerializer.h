#pragma once
#include "structs.h"
static class JsonRequestPacketSerializer {
public:
	static vector<char> serializeResponse(LoginResponse log);
	static vector<char> serializeResponse(SignupResponse sig);
	static vector<char> serializeResponse(ErrorResponse err);
	static vector<char> serializeResponse(LogoutResponse res);
	static vector<char> serializeResponse(Rooms rom);
	static vector<char> serializeResponse(PlayersInRoom pir);
	static vector<char> serializeResponse(CreateRoom res);
	static vector<char> serializeResponse(JoinRoom res);
	static vector<char> serializeResponse(UserStatistics ustat);
	static vector<char> serializeResponse(CloseRoomResponse close);
	static vector<char> serializeResponse(StartGameResponse start);
	static vector<char> serializeResponse(GetRoomStateResponse state);
	static vector<char> serializeResponse(LeaveRoomResponse leave);
private:
	static vector<char> dataToBuffer(int code, string data = "{status: 1}");
};
typedef JsonRequestPacketSerializer serializer;
