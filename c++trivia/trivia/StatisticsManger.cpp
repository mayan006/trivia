#include "StatisticsManger.h"

vector<string> StatisticsManger::getHighScore()
{
    return dataBase->getHighScore();
}

vector<double> StatisticsManger::getUserStatistics(string userName)
{
    vector<double> statistics;
    statistics.push_back(dataBase->getNumOfPlayerGames(userName));
    statistics.push_back(dataBase->getNumOfCorrectAnswer(userName));
    statistics.push_back(dataBase->getPlayerAverageAnswerTime(userName));
    return statistics;
}
