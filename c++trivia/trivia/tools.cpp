#include "tools.h"

void input(int& input){
	std::cin >> input;
	while (std::cin.fail()) {
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cout << "Bad entry.  Enter a NUMBER: ";
		std::cin >> input;
	}
	getchar();
}

vector<string> split(string str, string delimiter)
{
	vector<string> splitString;
	size_t pos = 0;
	std::string token;
	while ((pos = str.find(delimiter)) != std::string::npos) {
		token = str.substr(0, pos);
		splitString.push_back(token);
		str.erase(0, pos + delimiter.length());
	}
	splitString.push_back(str);
	return splitString;
}
