#include "JsonRequestPacketDeserializer.h"

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(vector<char> buffer)
{
    string bufferStr(buffer.begin(), buffer.end());
    LoginRequest structBuf;
    map<string, string> mapJson = splitJson(bufferStr);
    if (mapJson.find("username") == mapJson.end() || mapJson.find("password") == mapJson.end())
        throw std::exception("invaild massage format");
    structBuf.username = mapJson["username"];
    structBuf.password = mapJson["password"];
    return structBuf;
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(vector<char> buffer)
{
    std::string bufferStr(buffer.begin(), buffer.end());
    SignupRequest structBuf;
    map<string, string> mapJson = splitJson(bufferStr);
    if (mapJson.find("username") == mapJson.end() || mapJson.find("password") == mapJson.end() || mapJson.find("mail") == mapJson.end())
        throw std::exception("invaild massage format");
    structBuf.username = mapJson["username"];
    structBuf.password = mapJson["password"];
    structBuf.email = mapJson["mail"];
    return structBuf;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(vector<char> buffer)
{
    std::string bufferStr(buffer.begin(), buffer.end());
    GetPlayersInRoomRequest structBuf;
    map<string, string> mapJson = splitJson(bufferStr);    
    if (mapJson.find("roomId") == mapJson.end())
        throw std::exception("invaild massage format");
    structBuf.roomId = std::stoul(mapJson["roomId"],nullptr,0);
    return structBuf;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(vector<char> buffer)
{
    std::string bufferStr(buffer.begin(), buffer.end());
    JoinRoomRequest structBuf;
    map<string, string> mapJson = splitJson(bufferStr);
    if (mapJson.find("roomId") == mapJson.end())
        throw std::exception("invaild massage format");
    structBuf.roomId = std::stoul(mapJson["roomId"], nullptr, 0);
    return structBuf;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(vector<char> buffer)
{
    std::string bufferStr(buffer.begin(), buffer.end());
    CreateRoomRequest structBuf;
    map<string, string> mapJson = splitJson(bufferStr);
    if (mapJson.find("roomName") == mapJson.end() || mapJson.find("questionCount") == mapJson.end() 
        || mapJson.find("maxUsers") == mapJson.end() || mapJson.find("answerTimeout") == mapJson.end())
        throw std::exception("invaild massage format");
    structBuf.roomName = mapJson["roomName"];
    structBuf.questionCount = std::stoul(mapJson["questionCount"], nullptr, 0);
    structBuf.maxUsers = std::stoul(mapJson["maxUsers"], nullptr, 0);
    structBuf.answerTimeout = std::stoul(mapJson["answerTimeout"], nullptr, 0);
    return structBuf;
}


map<string, string> JsonRequestPacketDeserializer::splitJson(string json)
{
    if (json[0] != '{' || json[json.size() - 1] != '}')  throw std::exception("invaild json format");
    json.pop_back();
    json.erase(0, 1);
    vector<string> allKeys = split(json, ",");
    map<string, string> mapJson;
    for (string key : allKeys) {
        vector<string> splitKey = split(key, ":");
        if (splitKey.size() != 2) throw std::exception("invaild json format");
        mapJson[splitKey[0]] = splitKey[1];
    }
    return mapJson;
}
