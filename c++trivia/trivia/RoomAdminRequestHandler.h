#pragma once

#include "RoomMemeberRequestHandler.h"
class ReqestHandleFactory;
class RoomAdminRequestHandler : public RoomMemberRequestHandler {
public:

	RoomAdminRequestHandler(LoggedUser user, Room room, IRequestHandler* handler, ReqestHandleFactory* factory);
	virtual bool isRequestRelevant(RequestInfo requestInfo);
	virtual RequestResualt handleRequest(RequestInfo requestInfo);
private:
	RequestResualt startGame(RequestInfo reqestInfo);
	RequestResualt closeRoom(RequestInfo reqestInfo);

};
