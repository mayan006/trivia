#pragma once
#include<string>
#include<map>
#include<exception>
#include<time.h>
#include"tools.h"

using std::map;
using std::to_string;

enum RequstId{
	Login, Signup, Error, Logout, Join,
	Create, GetRoom, GetPlayers, HighScore, Close,
	Start, State, Leave
}typedef RequstId;

enum {
	numOfGams, CorrectAnswers, avargeTime
} typedef userStatistics;

struct Question {
	string question;
	string correctAnswer;
	string worngAnswer1;
	string worngAnswer2;
	string worngAnswer3;
}typedef Question;


struct RequestInfo {
	int id;
	time_t time;
	vector<char> buffer;
}typedef RequestInfo;

struct {
	string username;
	string password;
} typedef LoginRequest;

struct {
	string username;
	string password;
	string email;
} typedef SignupRequest;

struct {
	unsigned int roomId;
} typedef GetPlayersInRoomRequest;

struct {
	unsigned int roomId;
} typedef JoinRoomRequest;

struct {
	string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
} typedef CreateRoomRequest;

struct {
	int code;
} typedef defRequest;

struct {
	unsigned int status;
} typedef LoginResponse;

struct {
	unsigned int status;
} typedef CreateRoom;

struct {
	unsigned int status;
} typedef JoinRoom;

struct {
	unsigned int status;
} typedef LogoutResponse;

struct {
	vector<string> players;
} typedef PlayersInRoom;

struct {
	vector<string> highscore;
} typedef UserStatistics;


struct {
	unsigned int status;
} typedef SignupResponse;

struct {
	unsigned int status;
} typedef CloseRoomResponse;

struct {
	unsigned int status;
} typedef StartGameResponse;

struct {
	unsigned int status;
} typedef LeaveRoomResponse;

struct {
	string message;
} typedef ErrorResponse;

struct {
	unsigned int id;
	string name;
	unsigned int maxPlayers;
	unsigned int numOfQuestionslnGame;
	unsigned int timePerQuestion;
	bool isActive;
} typedef RoomData;

struct {
	unsigned int status;
	vector<RoomData> rooms;
}typedef Rooms;

struct {
	unsigned int status;
	bool hasGameBegun;
	vector<string> players;
	unsigned int questionCount;
	int answerTimeOut;
}typedef GetRoomStateResponse;
