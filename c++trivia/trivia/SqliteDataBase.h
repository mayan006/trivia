#pragma once
#include "IDataBase.h"
#include "sqlite3.h"
using std::pair;
class SqliteDataBase : public IDataBase {
public:
    SqliteDataBase();
    virtual bool doesUserExits(string username);
    virtual bool doesPasswordMatch(string username, string password);
    virtual bool addNewUser(string username, string password, string mail);
    virtual vector<Question> getQuestions(int numOfQuestion);
    virtual float getPlayerAverageAnswerTime(string username);
    virtual int getNumOfCorrectAnswer(string username);
    virtual int getNumOfTotalAnswer(string username);
    vector<int> getGameIds();
    virtual vector<string> getHighScore();
    virtual int getNumOfPlayerGames(string username);
    void addQuestion(Question  qustionAndAnswer, int id);
    static string strToInt(string str);
    virtual ~SqliteDataBase();

private:
    bool sendToDB(std::string massage, int(*callback)(void*, int, char**, char**) = nullptr, void* data = nullptr);
    virtual Question getQuestion(int questionId);
    static int insertDataCallback(void* data, int argc, char** argv, char** azColName);
    static int insertArrayCallback(void* data, int argc, char** argv, char** azColName);
    static int stringDataCallback(void* data, int argc, char** argv, char** azColName);
    static int questionDataCallback(void* data, int argc, char** argv, char** azColName);
    static int sumCallback(void* data, int argc, char** argv, char** azColName);
    static int countCallback(void* data, int argc, char** argv, char** azColName);
    int id;
    sqlite3* db;
};