#include "RoomManger.h"

void RoomManger::createRoom(LoggedUser user, Room room)
{
    room.addUser(user);
    rooms[room.getData().id] = room;
}

void RoomManger::deleteRoom(int roomID)
{
    rooms.erase(rooms.find(roomID));
}

Room* RoomManger::getRoom(int roomID)
{
    if(rooms.find(roomID) != rooms.end())
    return &rooms[roomID];
    return nullptr;
}

vector<RoomData> RoomManger::getRooms()
{
    vector <RoomData> roomVector;
    for (map<int, Room>::iterator it = rooms.begin(); it != rooms.end(); ++it) {
        roomVector.push_back(it->second.getData());
    }
    return roomVector;
}

Room RoomManger::joinRoom(LoggedUser user, int roomID)
{
    rooms[roomID].addUser(user);
    return rooms[roomId];
}

void RoomManger::LeaveRoom(LoggedUser user, int roomID)
{
    rooms[roomID].removeUser(user);
}

void RoomManger::StartGame(int roomID)
{
    rooms[roomID].startGame();
}

int RoomManger::getId()
{
    return roomId++;
}
