#include "Communicator.h"

static const unsigned short PORT = 1200;
static const unsigned int IFACE = 0;

Communicator::Communicator()
{
	dataBase = new SqliteDataBase();
	loginManger  = new LoginManger(dataBase);
	roomManger = new RoomManger();
	statstic = new StatisticsManger();
	factory = new ReqestHandleFactory(loginManger, roomManger, statstic, dataBase);;
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
	}
	isAaccept = true;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
	}
}

Communicator::~Communicator()
{
	try
	{
		delete loginManger;
		delete statstic;
		delete roomManger;
		delete dataBase;
		delete factory;
		closesocket(m_serverSocket);
		WSACleanup();
	}
	catch (...) {}
}

void Communicator::bindAndListen()
{
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
	}
	cout << "bind!" << endl;;
	freeaddrinfo(result);

	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
	};
	cout << "listen!" << endl;
}

void Communicator::startHandleRequests()
{

	SOCKET client_socket;
	try {
		bindAndListen();
		while (true) {
			if (isAaccept) {
				isAaccept = false;
				std::thread handleClientThread(&Communicator::handleNewClient, this);
				handleClientThread.detach();
			}
		}

	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was thrown in function: " << e.what() << std::endl;
		return;
	}
	// create new thread for client	and detach from it
}

RequestInfo Communicator::getMassege(SOCKET client)
{

		char buffer[1024];
		std::vector<char> reqVec;
		RequestInfo req;
		int len = recv(client, buffer, 1024, 0);
		if (len == SOCKET_ERROR) {
			string s = "socker error: ";
			s+= WSAGetLastError();
			throw std::exception(s.c_str());
		}
		if (buffer[0] == '1' && (buffer[1] == '1' || buffer[1] == '0' || buffer[1] == '2'))
		{
			req.id = (buffer[0] - TO_INT) * 10 + (buffer[1]- TO_INT);
		}
		else
		{
			req.id = buffer[0] - TO_INT;
		}
		req.buffer.assign(buffer + 5 , buffer + (len));
		return req;
}

void Communicator::handleNewClient() {


	SOCKET newClient;
	try {
		newClient = accept(ListenSocket, NULL, NULL);
		if (newClient == INVALID_SOCKET) {
			printf("accept failed with error: %d\n", WSAGetLastError());
			WSACleanup();
		}
		cout << "client accept!" << endl;
		isAaccept = true;
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was thrown in function: " << e.what() << std::endl;
		return;
	}
	// create new thread for client	and detach from it

	IRequestHandler* handler = new LoginRequestHandler(nullptr, factory);
	RequestResualt res;
	while (true) {
		try
		{
			RequestInfo req = getMassege(newClient);
			cout << "\n" << req.id << "\n";
			try {
				if (handler->isRequestRelevant(req)) {
					res = handler->handleRequest(req);
					handler = res.newHandler;
				}
				else {
					throw std::exception("invaild code");
				}
			}
			catch (const std::exception& e) {
				ErrorResponse err;
				err.message = e.what();
				res.response = JsonRequestPacketSerializer::serializeResponse(err);
			}
			char resp[1024];
			for (int i = 0; i < res.response.size(); i++)resp[i] = res.response[i];
			resp[res.response.size()] = 0;
			if (send(newClient, resp, res.response.size(), 0) == INVALID_SOCKET){//TODO i think we send only one bit
				string s = "socker error: ";
			s += WSAGetLastError();
			throw std::exception(s.c_str());
		}

		}

		catch (const std::exception& e)
		{
			cout << e.what();
			closesocket(newClient);
			delete handler;
			break;
		}
	}
}