#pragma once
#include "structs.h"
class IDataBase {
public:
	virtual bool doesUserExits(string) = 0;
	virtual bool doesPasswordMatch(string username, string password) = 0;
	virtual bool addNewUser(string username, string password, string mail) = 0;
	virtual vector<Question> getQuestions(int questionId) = 0;
	virtual vector<string> getHighScore() = 0;
	virtual float getPlayerAverageAnswerTime(string username) = 0;
	virtual int getNumOfCorrectAnswer(string username) = 0;
	virtual int getNumOfPlayerGames(string username) = 0;
	virtual int getNumOfTotalAnswer(string username) = 0;
	virtual ~IDataBase() = default;
};