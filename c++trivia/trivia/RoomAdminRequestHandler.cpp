#include "RoomAdminRequestHandler.h"
#include "RequestHandlerFactory.h"
RoomAdminRequestHandler::RoomAdminRequestHandler(LoggedUser user, Room room, IRequestHandler* handler, ReqestHandleFactory* factory):
    RoomMemberRequestHandler(user, room, handler, factory){

}
bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
    return requestInfo.id == Close  || requestInfo.id == Start || requestInfo.id == State;
}

RequestResualt RoomAdminRequestHandler::handleRequest(RequestInfo requestInfo)
{
    switch (requestInfo.id)
    {
    case Start:
        return startGame(requestInfo);

    case Close:
        return closeRoom(requestInfo);

    case State:
        return getState(requestInfo);
    }
}

RequestResualt RoomAdminRequestHandler::startGame(RequestInfo reqestInfo)
{
    RequestResualt ans;
    factory->getRoomManger()->StartGame(room.getData().id);
    ans.response = serializer::serializeResponse(StartGameResponse());
    ans.newHandler = this;
    return ans;
}

RequestResualt RoomAdminRequestHandler::closeRoom(RequestInfo reqestInfo)
{
    RequestResualt ans;
    factory->getRoomManger()->deleteRoom(room.getData().id);
    ans.response = serializer::serializeResponse(CloseRoomResponse());
    if (handler) {
        delete handler;
        handler = nullptr;
    }
    LoggedUser user(user);
    ans.newHandler = factory->createMenuRequestHandler(user, this);
    return ans;
}
