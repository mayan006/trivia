#include "IRequestHandler.h"

IRequestHandler::IRequestHandler(IRequestHandler* handler, ReqestHandleFactory* factory): handler(handler), factory(factory)
{
}

IRequestHandler::~IRequestHandler()
{
	if (handler) delete handler;
}
