#include "Room.h"

Room::Room(RoomData data):metadata(data)
{
}

void Room::addUser(LoggedUser loggedUser)
{
    users.push_back(loggedUser);
}

void Room::removeUser(LoggedUser loggedUser)
{
    vector<LoggedUser>::iterator userIt;
    for (userIt = users.begin(); userIt != users.begin(); userIt++) {
        if (userIt->getUserName() == loggedUser.getUserName()) break;
    }
    users.erase(userIt);
    //throw some exeption
}

vector<string> Room::getAllUsers()
{
    vector<string> strUsers;
    for (LoggedUser user : users) {
        strUsers.push_back(user.getUserName());
    }
    return strUsers;
}

RoomData Room::getData()
{
    return metadata;
}

void Room::startGame()
{
    metadata.isActive = true;
}

Room::~Room()
{
    users.clear();
}
