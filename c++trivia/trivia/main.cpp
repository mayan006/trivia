#pragma comment (lib, "ws2_32.lib")
#include "Server.h"
#include "SqliteDataBase.h"
#include "WSAInitializer.h"
#include "LoginRequestHandler.h"

int main()
{
	try
	{
		WSAInitializer wsa_init;
		server a;
		a.run();
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was thrown in function: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception in main !" << std::endl;
	}
}