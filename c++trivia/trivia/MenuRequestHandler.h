#pragma once

#include "LoginRequestHandler.h"
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class MenuRequestHandler : public IRequestHandler {
private:
	LoggedUser m_user;
	//ReqestHandleFactory* m_handlerFactory;
	RequestResualt signout(RequestInfo reqinfo);
	RequestResualt getRooms(RequestInfo reqinfo);
	RequestResualt getPlayersInRoom(RequestInfo reqinfo);
	RequestResualt getPersonalStat(RequestInfo reqinfo);
	RequestResualt getHighScore(RequestInfo reqinfo);
	RequestResualt joinRoom(RequestInfo reqinfo);
	RequestResualt createRoom(RequestInfo reqinfo);

public:
	MenuRequestHandler(LoggedUser user, IRequestHandler* handler, ReqestHandleFactory* factory);
	bool isRequestRelevant(RequestInfo reqinfo);
	RequestResualt handleRequest(RequestInfo reqinfo);
};
