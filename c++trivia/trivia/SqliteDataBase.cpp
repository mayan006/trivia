#include "SqliteDataBase.h"

SqliteDataBase::SqliteDataBase()
{
    id = 0;
    char* zErrMsg = 0;
    int rc;
    string sql;
    rc = sqlite3_open("trivia.db", &db);
    if (rc) std::cout << "Can't open database:" + string(sqlite3_errmsg(db)) + " \n";
    else std::cout << "Opened database successfully\n";
    sql = "CREATE TABLE USERS("  \
        "ID INT PRIMARY KEY     NOT NULL," \
        "USERNAME           TEXT    NOT NULL," \
        "PASSWORD           TEXT    NOT NULL," \
        "MAIL           TEXT    NOT NULL);";;
    if (sendToDB(sql)) std::cout << "Table USERS created successfully\n";
    sql = "CREATE TABLE STATISTICS("  \
        "ID_ANSWER INT PRIMARY KEY  NOT NULL," \
        "PLAYER_ID           INT    NOT NULL," \
        "GAME_ID             INT    NOT NULL," \
        "CORRECT             INT    NOT NULL," \
        "TIME                INT    NOT NULL);";
    if (sendToDB(sql)) std::cout << "Table STATISTICS created successfully\n";
    sql = "CREATE TABLE QUESTIONS("  \
        "QUESTIONS_ID INT PRIMARY KEY     NOT NULL," \
        "QUESTION            TEXT    NOT NULL," \
        "CURRECT_ANSWER      TEXT    NOT NULL," \
        "W_ANSWER1           TEXT    NOT NULL," \
        "W_ANSWER2           TEXT    NOT NULL," \
        "W_ANSWER3           TEXT    NOT NULL);";
    if (sendToDB(sql)) std::cout << "Table QUESTIONS created successfully\n";
}

bool SqliteDataBase::doesUserExits(string username)
{
    string sql = "SELECT ID FROM USERS WHERE ID == " + strToInt(username) + "; ";
    int id = 0;
    sendToDB(sql, countCallback, &id);
    if (id) {
        std::cout << "This user exits\n";
        return true;
    }
    return false;
}

bool SqliteDataBase::doesPasswordMatch(string username, string password)
{
    string sql = "SELECT PASSWORD FROM USERS WHERE ID == " + strToInt(username) + "; ";
    string id;
    sendToDB(sql, stringDataCallback, &id);
    if (id == password.c_str()) {
        std::cout << "This Password Match\n";
        return true;
    }
    return false;
}

bool SqliteDataBase::addNewUser(string username, string password, string mail)
{

    /* Create SQL statement */
    if (doesUserExits(username)) return false;
    string sql = "INSERT INTO USERS (ID,USERNAME,PASSWORD,MAIL) "  \
        "VALUES (" + strToInt(username) + ", '" + username + "', '" + password + "', '" + mail + "'); ";

    /* Execute SQL statement */

    if (sendToDB(sql)) {
        std::cout << "add user successfully\n";
        return true;
    }
    return false;
}

bool SqliteDataBase::sendToDB(std::string massage, int(*callback)(void*, int, char**, char**), void* data)
{
    bool flag = true;
    char* errMessage = nullptr;

    int res = sqlite3_exec(db, massage.c_str(), callback, data, &errMessage);
    if (res != SQLITE_OK) {
        std::cout << sqlite3_errmsg(db) << std::endl;
        flag = false;
    }
    delete[] errMessage;
    return flag;
}

int SqliteDataBase::insertDataCallback(void* data, int argc, char** argv, char** azColName)
{
    *(void**)data = *argv;
    return 0;
}

int SqliteDataBase::stringDataCallback(void* data, int argc, char** argv, char** azColName)
{
    *(string*)data = *argv;
    return 0;
}

SqliteDataBase::~SqliteDataBase()
{
    sqlite3_close(db);

}

string SqliteDataBase::strToInt(string str)
{
    string intstr;
    for (char c : str) {
        intstr += std::to_string((int)c);
    }
    return intstr;
}

vector<Question> SqliteDataBase::getQuestions(int numOfQuestion)
{
    vector<Question> questions;
   if(id <= numOfQuestion) throw std::exception("there is not enough question");
    for (int i = 0; i < numOfQuestion; i++) {
        questions.push_back(getQuestion(id));
    }
    return questions;
}

float SqliteDataBase::getPlayerAverageAnswerTime(string username)
{
    int sum;
    sendToDB("SELECT TIME FROM STATISTICS WHERE ID_PLAYER == " + strToInt(username) + ";", sumCallback, &sum);
    return sum / getNumOfCorrectAnswer(username);
}

int SqliteDataBase::getNumOfCorrectAnswer(string username)
{
    int count;
    sendToDB("SELECT TIME FROM STATISTICS WHERE ID_PLAYER = " + strToInt(username) + " AND CORRECT = 1;", countCallback, &count);
    return count;
}

int SqliteDataBase::getNumOfTotalAnswer(string username)
{
    int count;
    sendToDB("SELECT TIME FROM STATISTICS WHERE ID_PLAYER == " + strToInt(username) + ";", countCallback, &count);
    return count;
}

vector<string> SqliteDataBase::getHighScore()
{
    vector<string> playersId;
    vector<string> maxPlayers;
    double maxScore = 0;
    string maxPlayer;
    sendToDB("SELECT USERNAME FROM USERS", insertArrayCallback, &playersId);
    for (int i = 0; i < 5; i++) {
        maxScore = 0;
        for (string player : playersId) {
            double score = getNumOfTotalAnswer(player) / getNumOfCorrectAnswer(player);
            if (!score) continue;
            if (score > maxScore) {
                maxScore = score;
                maxPlayer = player;
            }
            else if (score == maxScore) {
                if (getPlayerAverageAnswerTime(player) < getPlayerAverageAnswerTime(maxPlayer)) {
                    maxScore = score;
                    maxPlayer = player;
                }
            }
        }
        playersId.erase(std::remove(playersId.begin(), playersId.end(), maxPlayer), playersId.end());
        maxPlayers.push_back(string("the players in the ") + to_string(i + 1) + 
            " place is: " + maxPlayer + " with:" + to_string(maxScore) + " points.");
    }
    return maxPlayers;
}


Question SqliteDataBase::getQuestion(int questionId)
{
    Question question;
    sendToDB("SELECT * FROM QUESTIONS WHERE QUESTIONS_ID == " + to_string(questionId) + 
        ";", questionDataCallback, &question);//TODO exeption
    return question;
}

vector<int> SqliteDataBase::getGameIds()
{
    int gameID = 0;
    vector<int> IDs;
    do
    {
        gameID = 0;
        string massage = "SELECT GAME_ID FROM STATISTICS" + string(IDs.empty() ? "" : "WHERE ");
        for (int id : IDs)massage += "GAME_ID != " + to_string(id) + " AND ";
        if(!IDs.empty())for (int i = 0; i < 4; i++)massage.pop_back();
        massage += ";";
        sendToDB(massage, insertDataCallback, &gameID);
        if (gameID) IDs.push_back(gameID);
    } while (gameID);
    return IDs;
}

/*vector<int> SqliteDataBase::getPlayersIds(int game_id)
{
    int playerID = 0;
    vector<int> IDs;
    do
    {
        playerID = 0;
        string massage = "SELECT PLAYER_ID FROM STATISTICS WHERE GAME_ID = " + to_string(game_id) + " AND ";
        for (int id : IDs)massage += "GAME_ID != " + to_string(id) + " AND ";
        for (int i = 0; i < 4; i++)massage.pop_back();
        massage += ";";
        sendToDB(massage, db, insertDataCallback, &playerID);
        if (playerID) IDs.push_back(playerID);
    } while (playerID);
    return IDs;
}*/


int SqliteDataBase::getNumOfPlayerGames(string username)
{
    return getGameIds().size();
}

void SqliteDataBase::addQuestion(Question qAndA, int id)
{
    sendToDB("INSERT INTO QUESTIONS (QUESTIONS_ID,CURRECT_ANSWER,W_ANSWER1,W_ANSWER2,W_ANSWER3) "  \
        "VALUES (" + to_string(id++) + ", '" + qAndA.correctAnswer + "', '" + qAndA.worngAnswer1 + "', '"
        + qAndA.worngAnswer2 + "', '" + qAndA.worngAnswer3 + "'); ");
}

int SqliteDataBase::insertArrayCallback(void* data, int argc, char** argv, char** azColName)
{
    ((vector<string>*)data)->push_back(*argv);
    return 0;
}

int SqliteDataBase::questionDataCallback(void* data, int argc, char** argv, char** azColName)
{
    Question* question = (Question*)data;
    for (int i = 0; i < argc; i++) {
        if (string(azColName[i]) == "CURRECT_ANSWER") {
            question->correctAnswer = argv[i];
        }
        else if (string(azColName[i]) == "W_ANSWER1") {
            question->worngAnswer1 = argv[i];
        }
        else if (string(azColName[i]) == "W_ANSWER2") {
            question->worngAnswer2 = argv[i];
        }
        else if (string(azColName[i]) == "W_ANSWER3") {
            question->worngAnswer3 = argv[i];
        }
        else if (string(azColName[i]) == "QUESTION") {
            question->question = argv[i];
        }
    }
    return 0;
}

int SqliteDataBase::sumCallback(void* data, int argc, char** argv, char** azColName)
{
    *(int*)data = atoi(argv[0]);
    return 0;
}

int SqliteDataBase::countCallback(void* data, int argc, char** argv, char** azColName)
{
    (*(int*)data)++;
    return 0;
}

