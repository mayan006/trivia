#pragma once

#include "structs.h"
#include "LoggedUser.h"

class Room {
public:
	Room() = default;
	Room(RoomData data);
	void addUser(LoggedUser loggedUser);
	void removeUser(LoggedUser loggedUser);
	vector<string> getAllUsers();
	RoomData getData();
	void startGame();
	~Room();
private:
	RoomData metadata;
	vector<LoggedUser> users;//TODO mayby cuse porblem

};
