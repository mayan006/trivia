#include "MenuRequestHandler.h"
#include "RequestHandlerFactory.h"


RequestResualt MenuRequestHandler::signout(RequestInfo reqinfo)
{
    RequestResualt ans;
    factory->getLoginManger()->logout(m_user.getUserName());
    ans.response = serializer::serializeResponse(LogoutResponse());
    if (handler) {
        delete handler;
        handler = nullptr;
    }
    ans.newHandler = factory->createLoginRequestHandler(this);
    return ans;
}

RequestResualt MenuRequestHandler::getRooms(RequestInfo reqinfo)
{
    RequestResualt ans;
    Rooms rooms;
    rooms.rooms = factory->getRoomManger()->getRooms();
    ans.response = serializer::serializeResponse(rooms);
    ans.newHandler = this;
    return ans;
}

RequestResualt MenuRequestHandler::getPlayersInRoom(RequestInfo reqinfo)
{
    RequestResualt ans;
    PlayersInRoom players;
    int id = (deserializer::deserializeGetPlayersInRoomRequest(reqinfo.buffer)).roomId;
    players.players = factory->getRoomManger()->getRoom(id)->getAllUsers();
    ans.response = serializer::serializeResponse(players);
    ans.newHandler = this;
    return ans;
}


RequestResualt MenuRequestHandler::joinRoom(RequestInfo reqinfo)
{
    RequestResualt ans;
    JoinRoomRequest join = deserializer::deserializeJoinRoomRequest(reqinfo.buffer);
    ans.response = serializer::serializeResponse(JoinRoom());
    if (handler) {
        delete handler;
        handler = nullptr;
    }
    LoggedUser user(m_user);
    ans.newHandler = factory->createMemberRequestHandler(user, factory->getRoomManger()->joinRoom(user, join.roomId), this);
    return ans;
}

RequestResualt MenuRequestHandler::createRoom(RequestInfo reqinfo)
{
    RequestResualt ans;
    CreateRoomRequest request = deserializer::deserializeCreateRoomRequest(reqinfo.buffer);
    RoomData roomD;
    roomD.timePerQuestion = request.answerTimeout;
    roomD.maxPlayers = request.maxUsers;
    roomD.name = request.roomName;
    roomD.numOfQuestionslnGame = request.questionCount;
    roomD.isActive = false;
    roomD.id = factory->getRoomManger()->getId();
    Room room(roomD);
    factory->getRoomManger()->createRoom(m_user, room);
    ans.response = serializer::serializeResponse(CreateRoom());
    if (handler) {
        delete handler;
        handler = nullptr;
    }
    ans.newHandler = factory->createAdminRequestHandler(LoggedUser(m_user), room, this);
    return ans;
}

MenuRequestHandler::MenuRequestHandler(LoggedUser user, IRequestHandler* handler, ReqestHandleFactory* factory):
    m_user(user), IRequestHandler(handler, factory)
{
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo reqinfo)
{
    return reqinfo.id == Logout  || reqinfo.id == Join || reqinfo.id == Create
        || reqinfo.id == GetRoom || reqinfo.id == GetPlayers;
}

RequestResualt MenuRequestHandler::handleRequest(RequestInfo reqinfo)
{
    switch (reqinfo.id)
    {
    case Logout:
        return signout(reqinfo);

    case Join:
        return joinRoom(reqinfo);
        

    case Create:
        return createRoom(reqinfo);
        

    case GetRoom:
        return getRooms(reqinfo);

    case GetPlayers:
        return getPlayersInRoom(reqinfo);
    }
}
