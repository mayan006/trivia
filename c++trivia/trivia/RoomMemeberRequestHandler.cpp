#include "RoomMemeberRequestHandler.h"
#include "RequestHandlerFactory.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(LoggedUser user, Room room, IRequestHandler* handler, ReqestHandleFactory* factory):
    IRequestHandler(handler, factory), user(user), room(room)
{
}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return requestInfo.id == Leave || requestInfo.id == State;
}

RequestResualt RoomMemberRequestHandler::handleRequest(RequestInfo requestInfo)
{
    switch (requestInfo.id)
    {
    case Leave:
        return LeaveRoom(requestInfo);

    case State:
        return getState(requestInfo);
    }
}

RequestResualt RoomMemberRequestHandler::LeaveRoom(RequestInfo reqestInfo)
{
    RequestResualt ans;
    factory->getRoomManger()->LeaveRoom(user, room.getData().id);
    ans.response = serializer::serializeResponse(LeaveRoomResponse());
    if (handler) {
        delete handler;
        handler = nullptr;
    }
    LoggedUser user(user);
    ans.newHandler = factory->createMenuRequestHandler(user, this);
    return ans;
}

RequestResualt RoomMemberRequestHandler::getState(RequestInfo reqestInfo)
{
    RequestResualt ans;
    Room* roomS = factory->getRoomManger()->getRoom(room.getData().id);
    if (roomS) {
        GetRoomStateResponse stateRes = { 1, roomS->getData().isActive, roomS->getAllUsers(),
            roomS->getData().numOfQuestionslnGame, roomS->getData().timePerQuestion };
        ans.response = serializer::serializeResponse(stateRes);
    }
    else {
        ans.response = serializer::serializeResponse(LeaveRoomResponse());
    }
    ans.newHandler = this;
    return ans;
}
