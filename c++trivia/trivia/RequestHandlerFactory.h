#pragma once

#include "LoginManger.h"
#include "StatisticsManger.h"
#include "RoomManger.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemeberRequestHandler.h"

class ReqestHandleFactory {
public:
	ReqestHandleFactory(LoginManger* loginManger, RoomManger* roomManger, StatisticsManger* statstic, IDataBase* iDataBase);
	LoginRequestHandler* createLoginRequestHandler(IRequestHandler* handler);
	MenuRequestHandler* createMenuRequestHandler(LoggedUser user, IRequestHandler* handler);
	RoomAdminRequestHandler* createAdminRequestHandler(LoggedUser user, Room room, IRequestHandler* handler);
	RoomMemberRequestHandler* createMemberRequestHandler(LoggedUser user, Room room, IRequestHandler* handler);
	LoginManger* getLoginManger();
	RoomManger* getRoomManger();
	StatisticsManger* getStatstic();
	IDataBase* getIDataBase();
private:
	LoginManger* loginManger;
	RoomManger* roomManger;
	StatisticsManger* statstic;
	IDataBase* iDataBase;
};