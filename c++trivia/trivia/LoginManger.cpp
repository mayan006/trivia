#pragma once
#include "LoginManger.h"

LoginManger::LoginManger(IDataBase* dataDase)
{
	m_data_base = dataDase;
}

void LoginManger::signup(string username, string password, string mail)
{
	m_data_base->addNewUser(username, password, mail);
	m_loggedUser.push_back(LoggedUser(username));
}

void LoginManger::login(string username, string password)
{
	if (m_data_base->doesUserExits(username) && m_data_base->doesPasswordMatch(username, password)) {
	m_loggedUser.push_back(LoggedUser(username));
	}
	else throw std::exception("username or password is currect");
		
}

void LoginManger::logout(string username)
{
	vector<LoggedUser>::iterator delUser;
	for (auto logIt = m_loggedUser.begin(); logIt != m_loggedUser.end();logIt++)
	{ if (logIt->getUserName() == username) delUser = logIt; }
	m_loggedUser.erase(delUser);//TODO check what the f*** going on with this code
}

LoginManger::~LoginManger()
{
}
