#pragma once
#include "IDataBase.h"

class StatisticsManger {
public:
	vector<string> getHighScore();
	vector<double> getUserStatistics(string userName);
private:
	IDataBase* dataBase;
};

