#include "LoggedUser.h"

LoggedUser::LoggedUser(std::string username):m_username(username)
{
}

std::string LoggedUser::getUserName()
{
    return m_username;
}
